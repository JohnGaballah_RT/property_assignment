

export const fetchProperties = data => ({
    type: 'FETCH_PROPERTIES',
    data
})

export const addToFavorites = data => ({
    type: 'ADD_TO_FAVORITES',
    data
})

export const removeFromFavorites = data => ({
    type: 'REMOVE_FROM_FAVORITES',
    data
})

