import React from 'react'
import {
    View,
    Text,
    StyleSheet
} from 'react-native'
import PropTypes from 'prop-types'


const CardHeader = ({ owner, price, single }) => {
    let backgroundColor = single ? '#fc6c85' : 'green'
    return (
        <View style={styles.priceView}>
            <View style={[styles.ownerChip, { backgroundColor }]}>
                <Text style={styles.ownerName}>
                    {owner}
                </Text>
            </View>
            <Text style={styles.price}>
                ${price}
            </Text>
        </View>
    )
}

export default CardHeader

CardHeader.propTypes = {
    owner: PropTypes.string,
    price: PropTypes.number,
    single: PropTypes.bool
}


const styles = StyleSheet.create({
    priceView: {
        marginBottom: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    ownerChip: {
        backgroundColor: 'green',
        borderRadius: 3,
        paddingHorizontal: 8,
        paddingVertical: 4
    },
    ownerName: {
        color: '#fff',
        fontSize: 12
    },
    price: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#000'
    }
})