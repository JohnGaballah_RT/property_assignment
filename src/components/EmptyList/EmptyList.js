import React from 'react'
import {
    View,
    Text,
    StyleSheet
} from 'react-native'
import en from '../../assets/locales/en.json'


const EmptyList = () => (
    <View style={styles.emptyView}>
        <Text style={styles.noResults}>
            {en.emptyList}
        </Text>
    </View>
)

export default EmptyList


const styles = StyleSheet.create({
    emptyView: {
        alignItems: 'center',
        paddingVertical: 34
    },
    noResults: {
        color: '#000',
        fontSize: 18,
        fontFamily: 'Arial'
    }
})