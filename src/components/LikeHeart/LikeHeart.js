import React from 'react'
import {
    StyleSheet,
    TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { withNavigation } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome';

import {
    addToFavorites,
    removeFromFavorites
} from '../../actions/index';




const LikeHeart = ({ propertyID, liked, addToFavorites, removeFromFavorites }) => {
    let toggleLike = () => liked ?
        removeFromFavorites(propertyID) :
        addToFavorites(propertyID)
    let heartColor = liked ? "#fc6c85" : "#000"
    return (
        <TouchableOpacity
            hitSlop={styles.actionHit}
            onPress={toggleLike}>
            <Icon
                name="heart"
                size={20}
                color={heartColor} />
        </TouchableOpacity>
    )
}



const mapStateToProps = (state, ownProps) => ({
    liked: state.favorites.find(each => each === ownProps.propertyID)
})

const mapDispatchToProps = dispatch => ({
    addToFavorites: data => dispatch(addToFavorites(data)),
    removeFromFavorites: data => dispatch(removeFromFavorites(data))
})


export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(LikeHeart))

LikeHeart.propTypes = {
    propertyID: PropTypes.number
}



const styles = StyleSheet.create({
    actionHit: {
        top: 20,
        bottom: 20,
        left: 20,
        right: 20
    }
})