

import React from 'react'
import {
    View,
    Text,
    StyleSheet
} from 'react-native'
import useNetInfo from './netInfoHook';
import en from '../../assets/locales/en.json';


const OfflineHint = () => {
    const isConnected = useNetInfo();
    if (isConnected) { return null }
    else {
        return (
            <View style={styles.mainView}>
                <Text style={styles.offlineMessage}>
                    {en.noInternet}
                </Text>
            </View>
        )
    }
}

export default OfflineHint

const styles = StyleSheet.create({
    mainView: {
        position: 'absolute',
        bottom: '6%',
        right: 0,
        left: 0,
        backgroundColor: 'red',
        alignItems: 'center',
        alignSelf: 'center',
        paddingVertical: 16,
        marginHorizontal: 10,
        borderRadius: 5,
        opacity: 0.9
    },
    offlineMessage: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 14
    }
})