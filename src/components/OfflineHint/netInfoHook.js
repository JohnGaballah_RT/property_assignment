
import { useState, useEffect } from 'react'
import  NetInfo  from '@react-native-community/netinfo'


function useNetInfo() {
    const [netInfo, setNetInfo] = useState(true)

    onChange = (state) => {
        setNetInfo(state.isConnected)
    }

    useEffect(() => {
        NetInfo.fetch().then(onChange)
        const unsubscribe = NetInfo.addEventListener(onChange)

        return () => unsubscribe();
    }, [])

    return netInfo
}

export default useNetInfo