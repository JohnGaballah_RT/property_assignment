import React, { useState } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Platform
} from 'react-native'
import PropTypes from 'prop-types'
import { TextInput } from 'react-native-gesture-handler';


const PriceFilter = ({ onTyping }) => {
    const [from, _setFrom] = useState('');
    const [to, _setTo] = useState('');
    let iosStyles = Platform.OS === 'ios' ? styles.iosStyles : null
    return (
        <View style={[
            styles.searchBarView,
            iosStyles
        ]}>
            <View style={styles.priceView}>
                <Text>$</Text>
                <TextInput
                    value={from}
                    placeholder={'0'}
                    keyboardType={'numeric'}
                    style={styles.searchText}
                    onChangeText={text => {
                        _setFrom(text)
                        onTyping(text, to)
                    }} />
            </View>
            <Text>-</Text>
            <View style={styles.priceView}>
                <Text>$</Text>
                <TextInput
                    value={to}
                    placeholder={'0'}
                    keyboardType={'numeric'}
                    style={styles.searchText}
                    onChangeText={text => {
                        _setTo(text)
                        onTyping(from, text)
                    }} />
            </View>
        </View>
    )
}

export default PriceFilter

PriceFilter.propTypes = {
    onTyping: PropTypes.func
}




const styles = StyleSheet.create({
    searchBarView: {
        height: 40,
        width: '38%',
        marginLeft: 16,
        marginRight: 5,
        marginBottom: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingRight: 16,
        borderRadius: 2,
        borderWidth: 1,
        borderColor: 'lightgrey'
    },
    iosStyles: {
        height: 40,
        marginTop: 20
    },
    searchText: {
        marginLeft: Platform.OS === 'ios' ? 3 : 0,
        color: '#000',
        fontSize: 14,
        fontFamily: 'Arial',
    },
    priceView: {
        flexDirection: 'row', 
        alignItems: 'center'
    }
})