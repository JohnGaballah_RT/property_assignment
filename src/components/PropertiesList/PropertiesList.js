
import React from 'react'
import { StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import { FlatList } from 'react-native-gesture-handler';
import PropertyCard from '../PropertyCard/PropertyCard';
import EmptyList from '../EmptyList/EmptyList';


const PropertiesList = ({ properties }) => {
    return (
        <FlatList
            data={properties}
            contentContainerStyle={styles.PropertiesList}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item) => item.id.toString()}
            ListEmptyComponent={<EmptyList />}
            renderItem={({ item }) =>
                <PropertyCard
                    propertyID={item.id} />
            }
        />
    )
}


export default PropertiesList

PropertiesList.propTypes = {
    properties: PropTypes.array
}



const styles = StyleSheet.create({
    PropertiesList: {
        marginTop: '5%',
        marginHorizontal: 16,
        borderRadius: 5,
    }
})