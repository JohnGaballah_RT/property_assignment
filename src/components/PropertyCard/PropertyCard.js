import React from 'react'
import {
    View,
    Text,
    Image,
    StyleSheet,
} from 'react-native'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { withNavigation } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome';

import Touchable from '../Touchable/Touchable';
import LikeHeart from '../LikeHeart/LikeHeart';
import en from '../../assets/locales/en.json'
import CardHeader from '../CardHeader/CardHeader';



const PropertyCard = ({ property, navigation }) => {
    let { id, owner, price, address, beds, bath } = property
    const navigateToDetails = () => navigation.navigate('propertyDetails', { id })
    return (
        <View>
            <Touchable
                onPress={navigateToDetails}
                contents={(
                    <View style={styles.properyView}>
                        <Image
                            resizeMethod={'resize'}
                            resizeMode={'cover'}
                            style={styles.propertyCover}
                            source={require('../../assets/images/apartment2.jpg')} />
                        <Icon
                            style={styles.bookmark}
                            name="bookmark"
                            size={20}
                            color="#fff" />

                    </View>
                )}
            />
            <View style={styles.detailsView}>
                <CardHeader
                    owner={owner}
                    price={price} />
                <View style={styles.likedView}>
                    <Text style={styles.address}>
                        {address} #3G
                            </Text>
                    <View style={styles.bedAndBathView}>
                        <View style={styles.numbersView}>
                            <Text style={styles.numberText}>
                                {beds}{en.bed}
                            </Text>
                            <Text style={styles.numberText}>
                                {bath}{en.bath}
                            </Text>
                        </View>
                        <LikeHeart propertyID={id} />
                    </View>
                </View>
            </View>
        </View>
    )
}

const getElementFromArray = (array, id) => {
    return array.find(element => element.id === id);
}

const mapStateToProps = (state, ownProps) => ({
    property: getElementFromArray(state.properties, ownProps.propertyID),
})


export default connect(mapStateToProps)(withNavigation(PropertyCard))

PropertyCard.propTypes = {
    propertyID: PropTypes.number
}



const styles = StyleSheet.create({
    properyView: {
        marginBottom: 80,
    },
    propertyCover: {
        backgroundColor: 'grey',
        borderRadius: 5,
        height: 200,
        width: '100%'
    },
    detailsView: {
        elevation: 3,
        position: 'absolute',
        bottom: '10%',
        borderRadius: 5,
        alignSelf: 'center',
        backgroundColor: '#fff',
        paddingTop: 10,
        paddingBottom: 16,
        paddingHorizontal: 8,
        width: '90%'
    },
    likedView: {
        marginLeft: 20
    },
    address: {
        fontSize: 18,
        color: '#000'
    },
    bedAndBathView: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    numbersView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    numberText: {
        fontSize: 14,
        fontWeight: '300',
        color: 'lightgrey'
    },
    bookmark: {
        position: 'absolute',
        right: 15,
        top: 15
    },
    actionHit: {
        top: 20,
        bottom: 20,
        left: 20,
        right: 20
    }
})