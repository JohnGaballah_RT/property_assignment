import React, { useState } from 'react'
import {
    View,
    StyleSheet,
    Platform
} from 'react-native'
import PropTypes from 'prop-types'
import { TextInput } from 'react-native-gesture-handler';
import en from '../../assets/locales/en.json';


const SearchBox = ({ onTyping }) => {
    const [search, _setSearch] = useState('');
    let iosStyles = Platform.OS === 'ios' ? styles.iosStyles : null
    return (
        <View style={[
            styles.searchBarView,
            iosStyles
        ]}>
            <TextInput
                value={search}
                placeholder={en.searchPlaceholder}
                style={styles.searchText}
                onChangeText={text => {
                    onTyping(text)
                    _setSearch(text)
                }} />
        </View>
    )
}

export default SearchBox

SearchBox.propTypes = {
    onTyping: PropTypes.func
}




const styles = StyleSheet.create({
    searchBarView: {
        height: 40,
        marginHorizontal: 16,
        marginBottom: 5,
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight: 16,
        borderRadius: 2,
        borderWidth: 1,
        borderColor: 'lightgrey'
    },
    iosStyles: {
        height: 40,
        marginTop: 20
    },
    searchText: {
        color: '#000',
        fontSize: 14,
        fontFamily: 'Arial',
        width: '100%',
        marginLeft: 10
    }
})