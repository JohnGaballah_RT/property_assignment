import React, { PureComponent } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Platform,
    TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import { FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from 'react-native-modal';


class SelectionFilter extends PureComponent {

    state = {
        optionsModal: false,
        valueChosen: 0
    }

    onCloseFilter = () => this.setState({ optionsModal: false })
    openModal = () => this.setState({ optionsModal: true })

    selectOption = option => {
        this.onCloseFilter()
        this.setState({
            valueChosen: option
        })
        this.props.onChoosen(option)
    }


    renderOptionsModal() {
        return (
            <Modal
                isVisible={this.state.optionsModal}
                onBackdropPress={this.onCloseFilter}
                onBackButtonPress={this.onCloseFilter}
                hideModalContentWhileAnimating={true}
                animationIn={'fadeInUp'}
                animationOut={'fadeOutDown'}
                useNativeDriver={true}>
                <View style={styles.modalMainView}>
                    {this.renderOptionsView()}
                </View>
            </Modal>
        )
    }

    renderOptionsView() {
        return (
            <FlatList
                data={this.props.options}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) =>
                    <TouchableOpacity
                        key={index}
                        activeOpacity={0.8}
                        onPress={() => this.selectOption(item)}
                        style={styles.eachOption}>
                        <Text>{item}</Text>
                    </TouchableOpacity>
                }
            />
        )
    }

    render() {
        let iosStyles = Platform.OS === 'ios' ? styles.iosStyles : null
        return (
            <View style={[
                styles.searchBarView,
                iosStyles
            ]}>
                {this.renderOptionsModal()}
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={this.openModal}
                    style={styles.selectionField}>
                    <Text>
                        {this.state.valueChosen}{this.props.criteria}
                    </Text>
                    <Icon
                        name="chevron-down"
                        size={15}
                        color="#000" />
                </TouchableOpacity>
            </View>
        )
    }
}

export default SelectionFilter

SelectionFilter.propTypes = {
    criteria: PropTypes.string,
    options: PropTypes.array,
    onChoosen: PropTypes.func
}




const styles = StyleSheet.create({
    searchBarView: {
        height: 40,
        width: '24%',
        marginBottom: 5,
        marginLeft: 5,
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 2,
        borderWidth: 1,
        borderColor: 'lightgrey'
    },
    iosStyles: {
        height: 40,
        marginTop: 20
    },
    selectionField: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        width: '100%',
        height: '100%'
    },
    modalMainView: {
        zIndex: 10,
        backgroundColor: '#fff',
        justifyContent: 'center',
        paddingVertical: 10,
        borderRadius: 12
    },
    eachOption: {
        paddingVertical: 10,
        paddingHorizontal: 30,
        marginBottom: 2,
    }
})