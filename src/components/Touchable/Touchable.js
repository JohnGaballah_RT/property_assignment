
import React from 'react'
import {
    Platform,
    TouchableNativeFeedback,
    TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'


const Touchable = props => {
    if (Platform.OS === 'android') {
        return (
            <TouchableNativeFeedback {...props}>
                {props.contents}
            </TouchableNativeFeedback>
        )
    } else {
        return (
            <TouchableOpacity {...props} activeOpacity={0.9}>
                {props.contents}
            </TouchableOpacity>
        )
    }
}

export default Touchable

Touchable.propTypes = {
    contents: PropTypes.element.isRequired
}