
import globals from '../utils/globals'


class Properties {

    async getPropertiesList() {
        try {
            let response = await fetch(
                `${globals.baseURL}`,
                {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json'
                    }
                });
            let responseJson = await response.json();
            return responseJson;
        } catch (error) {
            return false
        }
    }

}


export default Properties