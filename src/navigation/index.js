
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Home from '../views/home/home';
import PropertyDetails from '../views/propertyDetails/propertyDetails';


const MainNav = createStackNavigator({
    home: {
        screen: Home,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    },
    propertyDetails: {
        screen: PropertyDetails,
        navigationOptions: ({ navigation }) => ({
            header: null
        })
    }
}, {
        defaultNavigationOptions: {
            headerBackTitle: null
        }
    }
)


export default createAppContainer(MainNav)
