
let initialState = []


const FavoritesReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_TO_FAVORITES':
            return [
                ...state,
                action.data
            ]
        case 'REMOVE_FROM_FAVORITES':
            return [
                ...state.filter(favorite => favorite !== action.data)
            ]
        default:
            return state;
    }
}



export default FavoritesReducer
