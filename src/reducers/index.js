
import { combineReducers } from 'redux';

import PropertiesReducer from './properties';
import FavoritesReducer from './favorites';


const rootReducer = combineReducers({
    properties: PropertiesReducer,
    favorites: FavoritesReducer
})

export default rootReducer