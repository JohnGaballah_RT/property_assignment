
let initialState = []


const PropertiesReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_PROPERTIES':
            return [
                ...action.data
            ]
        default:
            return state;
    }
}



export default PropertiesReducer
