
const properties = [
    {
        "id": 1,
        "cover": "../../assets/images/apartment1.jpeg",
        "owner": "Williams Burg",
        "address": "14 Dunham PL",
        "price": 200,
        "details": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "short_id": 3,
        "beds": 1,
        "bath": 1.5
    },
    {
        "id": 2,
        "cover": "../../assets/images/apartment2.jpg",
        "owner": "Williams Burg",
        "address": "14 Brooklyn PL",
        "price": 400,
        "details": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "short_id": 4,
        "beds": 2,
        "bath": 1.5
    },
    {
        "id": 3,
        "cover": "../../assets/images/apartment3.jpg",
        "owner": "Williams Burg",
        "address": "14 Dunham PL",
        "price": 800,
        "details": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "short_id": 5,
        "beds": 3,
        "bath": 1.5
    },
    {
        "id": 4,
        "cover": "../../assets/images/apartment4.jpg",
        "owner": "Williams Burg",
        "address": "14 Dunham PL",
        "price": 1200,
        "details": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "short_id": 6,
        "beds": 4,
        "bath": 1.5
    }
]

export default properties