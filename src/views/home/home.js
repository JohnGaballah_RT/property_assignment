
import React, { Component } from 'react';
import {
    View,
    SafeAreaView,
    StatusBar,
    ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux'
import LinearGradient from 'react-native-linear-gradient';

import styles from './styles';
import SearchBox from '../../components/SearchBox/SearchBox';
import PropertiesList from '../../components/PropertiesList/PropertiesList';
import OfflineHint from '../../components/OfflineHint/OfflineHint';
import PriceFilter from '../../components/PriceFilter/PriceFilter';
import SelectionFilter from '../../components/SelectionFilter/SelectionFilter';
import Properties from '../../controllers/properties';
import { fetchProperties } from '../../actions/index';



class Home extends Component {

    constructor() {
        super()
        this.state = {
            loading: true,
            properties: [],
            toFilter: [],
            search: '',
            bedFilter: 0,
            bathFilter: 0,
            bedOptions: [1, 2, 3, 4, 5, 6],
            bathOptions: [1, 1.5, 2, 2.5, 3, 3.5]
        }
    }


    componentDidMount() {
        this.getProperties()
    }


    getProperties = async () => {
        let propertiesController = new Properties()
        let properties = await propertiesController.getPropertiesList()
        if (properties && Array.isArray(properties)) {
            this.props.fetchProperties(properties)
            this.setState({
                properties,
                toFilter: properties,
                loading: false
            })
        } else {
            this.getCachedProperties()
        }
    }

    getCachedProperties = () => this.setState({
        properties: this.props.properties,
        toFilter: this.props.properties,
        loading: false
    })


    handleSearch = text => {
        this.setState({
            search: text,
            properties: this.state.toFilter.filter(item => {
                return item.address.toLowerCase().search(text.toLowerCase()) !== -1;
            })
        });
    }

    bedChosen = value => {
        this.setState({
            bedFilter: value,
            properties: this.state.toFilter.filter(item => {
                return item.beds === value;
            })
        });
    }

    bathChosen = value => {
        this.setState({
            bathFilter: value,
            properties: this.state.toFilter.filter(item => {
                return item.bath === value;
            })
        });
    }

    filterPrice = (from, to) => {
        this.setState({
            properties: !from && !to ?
                this.state.toFilter :
                !from && to ?
                    this.state.toFilter.filter(item => {
                        return item.price <= to;
                    }) :
                    from && !to ?
                        this.state.toFilter.filter(item => {
                            return item.price >= from;
                        }) :
                        this.state.toFilter.filter(item => {
                            return item.price >= from && item.price <= to;
                        })
        })
    }


    renderStatusBar() {
        return (
            <StatusBar
                backgroundColor="#bcbcbf"
                barStyle="dark-content" />
        )
    }

    renderSearchBox() {
        return (
            <SearchBox
                onTyping={this.handleSearch} />
        )
    }

    renderFilters() {
        return (
            <View style={styles.filtersView}>
                <PriceFilter
                    onTyping={this.filterPrice} />
                <SelectionFilter
                    criteria={'br'}
                    chosen={this.state.bedFilter}
                    options={this.state.bedOptions}
                    onChoosen={this.bedChosen} />
                <SelectionFilter
                    criteria={'bh'}
                    chosen={this.state.bathFilter}
                    options={this.state.bathOptions}
                    onChoosen={this.bathChosen} />
            </View>
        )
    }

    renderScreenContents() {
        return this.state.loading ?
            this.renderPreloader() :
            this.renderPropertiesData()
    }

    renderPreloader() {
        return (
            <View style={styles.contentsView}>
                <ActivityIndicator
                    size='large'
                    color='#000' />
            </View>
        )
    }

    renderPropertiesData() {
        return (
            <View style={styles.contentsView}>
                <PropertiesList
                    properties={this.state.properties} />
            </View>
        )
    }


    render() {
        return (
            <SafeAreaView style={styles.container}>
                {this.renderStatusBar()}
                <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    style={styles.gradientBG}
                    colors={['#bcbcbf', '#e9eaec', '#FFFFFF']}>
                    {this.renderSearchBox()}
                    {this.renderFilters()}
                    {this.renderScreenContents()}
                </LinearGradient>
                <OfflineHint />
            </SafeAreaView>
        );
    }

}


const mapStateToProps = state => ({
    properties: state.properties
})

const mapDispatchToProps = dispatch => ({
    fetchProperties: data => dispatch(fetchProperties(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)