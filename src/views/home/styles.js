
import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#bcbcbf',
    },
    contentsView: {
        flex: 1, 
        justifyContent: 'center'
    },
    filtersView: {
        flexDirection: 'row', 
        alignItems: 'center'
    },
    gradientBG: {
        flex: 1, 
        paddingTop: 20
    }
})

export default styles