
import React, { Component } from 'react';
import {
    View,
    SafeAreaView,
    StatusBar,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/FontAwesome';

import styles from './styles'
import en from '../../assets/locales/en.json'
import OfflineHint from '../../components/OfflineHint/OfflineHint';
import LikeHeart from '../../components/LikeHeart/LikeHeart';
import CardHeader from '../../components/CardHeader/CardHeader';



class PropertyDetails extends Component {

    constructor() {
        super()
        this.state = {
            property: {},
            detailsExpanded: false,
            activeSection: 1
        }
    }


    componentDidMount() {
        this.getProperty()
    }


    getProperty = () => {
        if (this.props.navigation.state.params) {
            let { id } = this.props.navigation.state.params
            this.setState({
                property: this.props.properties.find(property => property.id === id)
            })
        }
    }

    goBack = () => this.props.navigation.goBack()

    toggleExpansion = () => this.setState({
        detailsExpanded: !this.state.detailsExpanded
    })

    activeFirst = () => this.setState({ activeSection: 1 })
    activeSecond = () => this.setState({ activeSection: 2 })
    activeThird = () => this.setState({ activeSection: 3 })




    renderStatusBar() {
        return (
            <StatusBar
                backgroundColor="#bcbcbf"
                barStyle="dark-content" />
        )
    }


    renderHeader() {
        return (
            <View>
                <Image
                    resizeMethod={'resize'}
                    resizeMode={'cover'}
                    style={styles.propertyCover}
                    source={require('../../assets/images/apartment2.jpg')} />
                <TouchableOpacity
                    onPress={this.goBack}
                    style={styles.backAction}
                    activeOpacity={0.8}
                    hitSlop={styles.actionHit}>
                    <Icon
                        name="chevron-left"
                        size={18}
                        color="#000" />
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.bookmarkAction}
                    activeOpacity={0.8}
                    hitSlop={styles.actionHit}>
                    <Icon
                        name="bookmark"
                        size={20}
                        color="#fff" />
                </TouchableOpacity>
            </View>
        )
    }


    renderDetailsCard() {
        let { id, owner, price, address, details } = this.state.property
        let propertyDetails = this.state.detailsExpanded ?
            <Text>{details}</Text> :
            <Text numberOfLines={3}>{details}</Text>
        return (
            <View style={styles.detailsView}>
                <CardHeader
                    single
                    owner={owner}
                    price={price} />
                <View style={styles.likedView}>
                    <View style={styles.bedAndBathView}>
                        <Text style={styles.address}>
                            {address} #3G
                        </Text>
                        <LikeHeart propertyID={id} />
                    </View>
                    {propertyDetails}
                    {this.renderReadLink()}
                </View>
                {this.renderSections()}
            </View>
        )
    }

    renderReadLink() {
        let readLink = `${en.read} ${this.state.detailsExpanded ? en.less : en.more}`
        return (
            <TouchableOpacity
                activeOpacity={0.8}
                hitSlop={styles.actionHit}
                onPress={this.toggleExpansion}>
                <Text style={styles.readMore}>
                    {readLink}
                </Text>
            </TouchableOpacity>
        )
    }

    renderSections() {
        let firstActive = this.state.activeSection === 1 ? styles.activeSection : null
        let secondActive = this.state.activeSection === 2 ? styles.activeSection : null
        let thirdActive = this.state.activeSection === 3 ? styles.activeSection : null
        return (
            <View style={styles.sectionsView}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={[styles.eachSection, firstActive]}
                    onPress={this.activeFirst}>
                    <Text style={styles.sectionTitle}>
                        {en.details}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={[styles.eachSection, secondActive]}
                    onPress={this.activeSecond}>
                    <Text style={styles.sectionTitle}>
                        {en.amenities}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={[styles.eachSection, thirdActive]}
                    onPress={this.activeThird}>
                    <Text style={styles.sectionTitle}>
                        {en.rooms}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                {this.renderStatusBar()}
                {this.renderHeader()}
                {this.renderDetailsCard()}
                <OfflineHint />
            </SafeAreaView>
        );
    }

}


const mapStateToProps = state => ({
    properties: state.properties,
    favorites: state.favorites
})


export default connect(mapStateToProps)(PropertyDetails)