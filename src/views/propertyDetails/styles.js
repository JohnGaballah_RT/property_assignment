
import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e9eaec',
    },
    propertyCover: {
        height: 250,
        width: '100%'
    },
    detailsView: {
        elevation: 3,
        position: 'absolute',
        top: '30%',
        borderRadius: 5,
        alignSelf: 'center',
        backgroundColor: '#fff',
        padding: 16,
        width: '90%'
    },
    likedView: {
        marginLeft: 20
    },
    address: {
        fontSize: 18,
        color: '#000'
    },
    bedAndBathView: {
        marginVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    backAction: {
        position: 'absolute',
        left: 20,
        top: 20,
        zIndex: 2
    },
    bookmarkAction: {
        position: 'absolute',
        right: 20,
        top: 20,
        zIndex: 2
    },
    actionHit: {
        top: 20,
        bottom: 20,
        left: 20,
        right: 20
    },
    readMore: {
        marginTop: 10,
        textAlign: 'right',
        color: 'grey'
    },
    sectionsView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 15,
        borderTopColor: 'lightgrey',
        borderTopWidth: 1
    },
    eachSection: {
        paddingTop: 15,
        flex: 1
    },
    activeSection: {
        borderTopColor: '#3498DB',
        borderTopWidth: 2
    },
    sectionTitle: {
        color: '#000',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center'
    }
})

export default styles